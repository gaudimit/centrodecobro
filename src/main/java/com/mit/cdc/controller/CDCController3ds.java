package com.mit.cdc.controller;

@RestController
public class CDCController {

	
	@Autowired
	CobroService cobroService;
	
	@PostMapping(path="/cobrototal")
	public  double getCobroTotal(@RequestBody double importe) {	
		return cobroService.cobroTotal(importe + ((importe/100)*16));
	}
	
	@PostMapping(path="/agregarpropinas")  
	public double agregarpropina(double propina) {	
		return cobroService.agregarpropina(propina);
	}

}
